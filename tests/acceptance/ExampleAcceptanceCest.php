<?php


class ExampleAcceptanceCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->installModule();
    }

    public function _after(AcceptanceTester $I)
    {
        $I->uninstallModule();
    }

    // tests
    public function openConfigurationPage(AcceptanceTester $I)
    {
        $I->loginAsAdmin();
        $I->amOnConfigurationPage();

        $I->see('A TEST MODULE FOR PRESTASHOP');
    }
}
